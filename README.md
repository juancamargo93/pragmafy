# Pragmafy

It's a simple spotify API wrapper

### Installing

A step by step series of examples that tell you how to get a development env running

First, you need to install nodejs, you can download from

https://nodejs.org/es/


Then need to install React Native. Can go to the next link and select the tab "Building Projects with Navie Code" To get started

https://facebook.github.io/react-native/docs/getting-started


To install Android dependencies need to open the project, no the root folder, have to open the "android" folder inside the root project.


## Getting Started

To get started have to install modules with next command

```
npm install
```

For Android have to run adb device before run project. iOS doesn't have to.
To get started you need to clone the repo, switch to the development branch. Follow the next steps and run it

```
react-native run-android
```
or

```
react-native run-ios
```

## Running the tests

To run test

```
npm test
```

## Deployment

To generate a signed APK check the following link

https://facebook.github.io/react-native/docs/0.57/signed-apk-android

And for ios if want to generate an app (ipa) for production have to check here

https://facebook.github.io/react-native/docs/0.57/running-on-device#building-your-app-for-production


## Built With

* [ReactNative](https://facebook.github.io/react-native/docs/0.57/getting-started.html)
* [npm](https://www.npmjs.com/) - Dependency Management


## Authors

* **Juan Camargo** - *Initial work*

## License

This project is licensed under the MIT License
