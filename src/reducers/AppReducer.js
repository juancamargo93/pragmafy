import { combineReducers } from 'redux'
import NavReducer from './NavReducer'
import playStopReducer from './playStopReducer'
/**
 * Combine the reducers and export it in one
 */
const AppReducer = combineReducers({
  nav: NavReducer,
  play: playStopReducer
})

export default AppReducer
