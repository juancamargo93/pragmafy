import { PLAY_URL, STOP_TRACK } from '../actions/types'

const initialState = {
  isPlaying: false,
  isPaused: false,
  url: '',
  fromMain: false,
  stop: false
}

const playStopReducer = (state = initialState, action) => {
  switch (action.type) {
    case PLAY_URL:
      return {
        ...state,
        url: action.payload.url,
        isPlaying: action.payload.play,
        isPaused: action.payload.pause,
        fromMain: action.payload.fromMain,
        stop: action.payload.stop
      }
    case STOP_TRACK:
      return {
        ...state,
        stop: action.payload.stop,
        fromMain: action.payload.fromMain,
        isPlaying: action.payload.play
      }
    default:
      return state
  }
}

export default playStopReducer
