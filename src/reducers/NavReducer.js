import { AppNavigator } from '../components/AppNavigator'
import {
  createNavigationReducer
} from 'react-navigation-redux-helpers'

const NavReducer = createNavigationReducer(AppNavigator)

export default NavReducer
