import { createStore, combineReducers } from 'redux'
import reducer from '../reducers/reducer'

const rootReducer = combineReducers({
  play: reducer
})

const configureStore = () => {
  return createStore(rootReducer)
}

export default configureStore
