import React from 'react'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Settings from '../screens/components/settings'
import Documentation from '../screens/containers/documentation'
import Main from '../screens/components/main'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {
  createReactNavigationReduxMiddleware
} from 'react-navigation-redux-helpers'
/**
 * Manage the route system
 */
export const AppNavigator = createMaterialBottomTabNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: {
        title: 'Search',
        tabBarLabel: 'Search',
        tabBarColor: '#FF5D00',
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon size={25} name='magnify' color='#fff' />
        )
      }
    },
    About: {
      screen: Settings,
      navigationOptions: {
        title: 'About',
        tabBarLabel: 'About',
        tabBarColor: '#FF9100',
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon size={25} name='information' color='#fff' />
        )
      }
    },
    Api: {
      screen: Documentation,
      navigationOptions: {
        title: 'Api Spotify',
        tabBarLabel: 'Api Spotify',
        tabBarColor: '#FFDB03',
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon size={25} name='spotify' color='#fff' />
        )
      }
    }
  },
  {
    activeColor: '#FFF',
    inactiveColor: '#FFF',
    shifting: true,
    barStyle: { backgroundColor: '#3D6DCC' },
    indicatorStyle: {
      alignSelf: 'center'
    }
  }
)

export const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
)