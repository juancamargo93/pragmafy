import 'react-native'
import React from 'react'
import { Main } from '../main'
import API from '../../../../utils/api'

import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import configureStore from 'redux-mock-store'
import { playUrl } from '../../../actions/plaStopAction'
import reducer from '../../../reducers/playStopReducer'

const mockStore = configureStore()

const initialState = {
  isPlaying: false,
  isPaused: false,
  url: '',
  stop: false,
  fromMain: false
}

configure({ adapter: new Adapter() })
describe('Testing Main component', () => {
  it('Main renderers', () => {
    const wrapper = shallow(
      <Main />
    )
    expect(wrapper).toMatchSnapshot()
  })
})

describe('Testing requests', () => {
  const store = mockStore(initialState)
  let token = ''

  beforeEach(() => {
    store.clearActions()
  })

  it('save playUrl', async () => {
    await store.dispatch(playUrl('', false, false, false, false))
    expect(store.getActions()).toMatchSnapshot()
  })

  it('get token', async () => {
    const res = await API.getToken()
    token = res.access_token
    expect(res).toBeTruthy()
  })

  it('get artists search', async () => {
    const searchRes = await API.searchArtist(token, 'a')
    expect(searchRes).toBeTruthy()
  })

  it('retireve data', async () => {
    const wrapper = shallow(<Main/>)
    const instance = wrapper.instance()
    const res = await instance.retrieveData()
    expect(res).toBeTruthy()
  })

  it('should play or pause current track', async () => {
    const wrapper = shallow(<Main/>)
    const instance = wrapper.instance()
    const res = await instance.retrieveData()
    const previewUrl = res.filter(data => data.hasOwnProperty('previewUrl'))[0].previewUrl
    await instance.playPauseTrack(previewUrl, false, false)
    expect(store.getState().isPlaying).toBe(false)
  })
})

describe('Post reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).not.toEqual({})
  })

  it('should handle PLAY_URL', () => {
    const startAction = {
      type: 'playUrl',
      payload: { url: undefined }
    }
    expect(reducer({}, startAction)).toEqual({})
  })

  it('should handle PLAY_URL success', () => {
    const playAction = {
      type: 'playUrl',
      payload: { url: '', play: true, pause: false, fromMain: false, stop: false }
    }
    expect(reducer({}, playAction)).toEqual({ url: '', isPlaying: true, isPaused: false, fromMain: false, stop: false })
  })

  it('shoud handle STOP_URL', () => {
    const stopAction = {
      type: 'stopTrack',
      payload: { stop: undefined }
    }
    expect(reducer({}, stopAction)).toEqual({})
  })

  it('should handle STOP_URL success', () => {
    const stopAction = {
      type: 'stopTrack',
      payload: { stop: true, fromMain: false, play: false }
    }
    expect(reducer({}, stopAction)).toEqual({ stop: true, fromMain: false, isPlaying: false })
  })
})
