import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Avatar } from 'react-native-paper'
import DeviceInfo from 'react-native-device-info'

class Settings extends Component {
  static navigationOptions = {
    header: null
  }
  render () {
    return (
      <View style={styles.container}>
        <Avatar.Image
          size={200}
          source={require('../../images/pragma.jpg')}
        />
        <Text style={styles.versionText}>
          App Version: { DeviceInfo.getVersion() }
        </Text>
        <Text style={styles.developedText}>Developed By: Juan Camargo</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  versionText: {
    fontSize: 22,
    margin: 10
  },
  developedText: {
    fontSize: 19,
    margin: 10
  }
})

export default Settings
