import React, { Component } from 'react'
import Home from '../containers/home'
import CardComponent from '../containers/card'
import API from '../../../utils/api'
import { playUrl } from '../../actions/playStopAction'
import Store from '../../../utils/store'
import { FlatList, StyleSheet } from 'react-native'
import { SearchBar } from 'react-native-elements'
import Sound from 'react-native-sound'
import { connect } from 'react-redux'

/**
 * Main component, content the logic and also make the http request to the API.
 * Render the Card component from a FlatList
 */
export class Main extends Component {
  static navigationOptions={
    header: null
  }
  constructor (props) {
    super(props)
    this.state = {
      /** Array, content the result of the search to the Spotify API */
      search: [],
      /** Flag variable to display or hide the spinner (loader) */
      loading: false,
      /** Content the search text */
      searchText: '',
      /** Content the url, is used to compare the current url (playing) song with a new one */
      url: '',
      /** Sound, references to the sound Object, with this can manage the sound */
      sound: {}
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.stop) this.state.sound.stop()
    this.setState({ url: nextProps.url }, () => {
      if (!nextProps.fromMain) {
       this.playPauseTrack(nextProps.url, nextProps.isPlaying, nextProps.isPaused, false, false)
      }
    })
  }
  /**
   * Manage the logic to play and resume a song
   * @param {String} url
   * @param {Boolean} isPlaying
   * @param {isPaused} Boolean
   */
  playPauseTrack = async (url, isPlaying, isPaused) => {
    if (isPlaying) {
      this.state.sound.getCurrentTime((seconds) => {
        if (seconds > 0) {
          isPaused ? this.state.sound.play() : this.state.sound.pause()
          this.props.playUrl(this.state.url, true, !this.props.isPaused, true, false)
        }
      })
    } else {
      const sound = new Sound(url, undefined,
        (error) => {
          if (error) {
            console.log(error)
            return
          }
          this.props.playUrl(this.state.url, true, false, true, false)
          this.setState({ sound: sound })
          sound.play(() => {
          })
        })
    }
  }
  /**
   * Manage input event and set the state searchText
   * @param {String} text
   */
  changeText = async (text) => {
    try {
      let searchText = text
      if (text === '') searchText = 'a'
      this.setState({ searchText: text })
      const res = await this.retrieveData(searchText)
      this.setState({ search: res })
    } catch (error) {
      console.log(error)
    }
  }
  /**
   * ask if access_token exists and if is valid, if it's make the request from the service API if not refresh the token
   * Reesponse Array of objects with data
   * @param {String} value 
   */
  async retrieveData (value = 'a') {
    this.setState({loading: true})
    let token = await Store.getData('oauthToken')
    let search = {}
    if (!token) {
      token = await API.getToken()
      await Store.saveData('oauthToken', token.access_token)
      search = await API.searchArtist(token.access_token, value)
    } else {
      search = await API.searchArtist(token, value)
      if (search.hasOwnProperty('error')) {
        token = await API.getToken()
        await Store.saveData('oauthToken', token.access_token)
        search = await API.searchArtist(token, value)
      }
    }
    this.setState({loading: false})
    let combineSearch = []
    search.albums.items.forEach(element => {
      if (element.album_type === 'album') combineSearch.push({ id: element.id, external_urls: element.external_urls.spotify, uri: element.uri, images: element.images, href: element.href, name: element.name, type: element.type })
    })
    search.tracks.items.forEach(element => {
      combineSearch.push({ id: element.id, external_urls: element.external_urls.spotify, uri: element.uri, images: element.album.images, href: element.href, name: element.name, type: element.type, previewUrl: element.preview_url })
    })
    search.artists.items.forEach(element => {
      combineSearch.push({ id: element.id, external_urls: element.external_urls.spotify, uri: element.uri, images: element.images, href: element.href, name: element.name, type: element.typem })
    })
    return combineSearch
  }
  
  async componentDidMount () {
    const res = await this.retrieveData()
    this.setState({ search: res })
  }
  render () {
    return (
      <Home>
        <SearchBar 
          containerStyle={styles.searchcontainer}
          inputStyle={{ backgroundColor: '#EDF6FC' }}
          value={this.state.searchText}
          clearIcon={this.state.searchText !== '' ? { name: 'cancel' } : false}
          searchIcon={true}
          placeholder='Type here...'
          onChangeText={this.changeText}
          showLoadingIcon={this.state.loading}
        />
        <FlatList
          data={this.state.search}
          renderItem={({item}) => <CardComponent data={item} />}
          keyExtractor={(item, index) => index.toString()}
          initialNumToRender={this.state.search.length}
        />
      </Home>
    )
  }
}

const styles = StyleSheet.create({
  searchcontainer: {
    backgroundColor: '#FFF',
    borderWidth: 0, //no effect
    shadowColor: 'white', //no effect
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent'
   }
})
/**
* Redux native function to use dispatch to call action functions
*/
function mapDispatchToProps (dispatch) {
  return {
    playUrl: (url, play, pause, fromMain, stop) => {
      dispatch(playUrl(url, play, pause, fromMain, stop))
    }
  }
}
/**
 * Redux native function to use state redux as props
 */
function mapStateToProps (state) {
  return {
    isPlaying: state.play.isPlaying,
    isPaused: state.play.isPaused,
    url: state.play.url,
    stop: state.play.stop,
    fromMain: state.play.fromMain
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
