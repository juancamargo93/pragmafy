import 'react-native'
import React from 'react'

import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import configureStore from 'redux-mock-store'
import { CardComponent } from '../card'
import { Main } from '../../components/main'


const mockStore = configureStore()
const store = mockStore(initialState)

const initialState = {
  isPlaying: false,
  isPaused: false,
  url: '',
  stop: false,
  fromMain: false
}

configure({ adapter: new Adapter() })

describe('Should sanp component', () => {
  let instance = ''
  let searchRes = ''
  beforeEach(async () => {
    store.clearActions()
    const wrapperMain = shallow(<Main />)
    instance = wrapperMain.instance()
    searchRes = await instance.retrieveData()
  })

  it('should retrieve data and render component', async () => {
    const wrapper = shallow(<CardComponent data={searchRes[0]} />)
    expect(wrapper).toMatchSnapshot()
  })

})
