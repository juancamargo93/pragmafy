import React, { Component } from 'react'
import { connect } from 'react-redux'
import { playUrl, stopTrack } from '../../actions/playStopAction'
import { Button, Card, Title } from 'react-native-paper'
import { View, Linking, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export class CardComponent extends Component {

  constructor (props) {
    super(props)
    this.state = {
      /** Icon state */
      icon: 'play',
      /** Flag state, is used to know when change the icon */
      iconChange: false
    }
  }
  /**
   * Handle event and set states to play
   * @param {String} url
   */
  handlePlayButton = async (url) => {
    if (this.props.url !== url) {
      if (this.props.url.length > 0) await this.handleStopButton()
    }
    this.props.playUrl(url, this.props.isPlaying, this.props.isPaused, false, false)
    this.setState({ iconChange: !this.state.iconChange, icon: this.state.iconChange ? 'play' : 'pause' })
  }
  /**
   * Stop current song
   */
  handleStopButton = async () => {
    this.props.stopTrack(true, true, false)
    this.setState({ icon: 'play', iconChange: false })
  }

  render () {
    return (
      <Card style={styles.cardContainer} elevation={3}>
        <Card.Content style={styles.cardContent}>
          <Title>{this.props.data.name}</Title>
        </Card.Content>
        <Card.Cover source={{ uri: this.props.data.images.length > 0 ? this.props.data.images[1].url : 'https://d500.epimg.net/cincodias/imagenes/2018/04/16/smartphones/1523872346_850117_1523872532_noticia_normal.jpg' }} />
        <Card.Actions style={styles.cardActions}>
          <View style={styles.viewActionText}>
            <Button color='#FF9100' onPress={() => { Linking.openURL(this.props.data.external_urls) }}>Open On Spotify</Button>
          </View>
          <View style={styles.viewActionButton}>
            { this.renderPlayIcon() }
            { this.renderStopIcon() }
          </View>
        </Card.Actions>
      </Card>
    )
  }

  /**
   * Return a play icon
   */
  renderPlayIcon = () => {
    let icon
    let self = this
    if (this.props.data.previewUrl) {
      icon = (
          <Button onPress={() => self.handlePlayButton(this.props.data.previewUrl)}>
            <Icon size={25} name={this.state.icon} color='#747474' />
          </Button>
      )
    }
    return icon
  }
  /**
   * Return a stop icon
   */
  renderStopIcon = () => {
    let icon
    if (this.props.data.previewUrl) {
      icon = (
        <Button onPress={() => this.handleStopButton()}>
          <Icon size={25} name='stop' color='#747474' />
        </Button>
      )
    }
    return icon
  }
}
/**
 * Redux native function to use dispatch to call action functions
 */
function mapDispatchToProps (dispatch) {
  return {
    playUrl: (url, play, pause, fromMain, stop) => {
      dispatch(playUrl(url, play, pause, fromMain, stop))
    },
    stopTrack: (stop) => {
      dispatch(stopTrack(stop))
    }
  }
}
/**
 * Redux native function to use state redux as props
 */
function mapStateToProps (state) {
  return {
    isPlaying: state.play.isPlaying,
    isPaused: state.play.isPaused,
    url: state.play.url,
    stop: state.play.stop
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    marginBottom: 5,
    marginTop: 5,
    marginRight: 10,
    marginLeft: 10
  },
  cardContent: {
    marginBottom: 10
  },
  cardActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewActionText: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  viewActionButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(CardComponent)
