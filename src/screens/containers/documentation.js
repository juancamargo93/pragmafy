import React, { Component } from 'react'
import { WebView } from 'react-native'

/**
 * Render website from url
 */
export default class Documentation extends Component {

  render () {
    return (
      <WebView
        source={{ uri: 'https://developer.spotify.com/documentation/web-api/reference/' }}
        style={{ marginTop: 20 }}
      />
    )
  }
}