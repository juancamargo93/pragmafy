import { PLAY_URL, STOP_TRACK } from './types'

/**
 *
 * @param {String} url 
 * @param {Boolean} play 
 * @param {Boolean} pause 
 * @param {Boolean} fromMain 
 * @param {Boolean} stop 
 */
export function playUrl (url, play, pause, fromMain, stop) {
  return {
    type: PLAY_URL,
    payload: { url, play, pause, fromMain, stop }
  }
}
/**
 * 
 * @param {Boolean} stop 
 * @param {Boolean} fromMain 
 * @param {Boolean} play 
 */
export function stopTrack (stop, fromMain = true, play = false) {
  return {
    type: STOP_TRACK,
    payload: { stop, fromMain, play }
  }
}
