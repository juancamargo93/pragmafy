const BASE_API = 'https://api.spotify.com/v1'
const BASE_TOKEN = 'https://accounts.spotify.com/api'
const CLIENT_ID = '8015715f34ce4d499e6873a1a186b013'
const CLIENT_SECRET = '7863061d80b842b9af6f06e9c523340f'

import base64 from 'react-native-base64'
import axios from 'axios'
/**
 * Manage the requests to get token and get searchs from Spotify
 */
class Api {
  async getToken () {
    let credentials = base64.encode(CLIENT_ID+':'+CLIENT_SECRET)
    const res = await axios({ url: `${BASE_TOKEN}/token`, method: 'POST', headers: { 'Authorization': `Basic ${credentials}`, 'Content-Type': 'application/x-www-form-urlencoded' }, data: 'grant_type=client_credentials'})
    return res.data
  }
  async searchArtist (token, name) {
    const res = await axios({ url: `${BASE_API}/search?q=album:${name}%20OR%20artist:${name}%20OR%20name:${name}&type=album,artist,track&market=CO&offset=0&limit=20`, headers: { 'Authorization': `Bearer ${token}` } })
    return res.data
  }
}

export default new Api()
