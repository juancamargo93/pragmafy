
import { AsyncStorage } from 'react-native'
/**
 * Manage the events to save and get data from AsyncStorage
 */
class Store {
  async saveData (key, value) {
    try {
      await AsyncStorage.setItem(key, value)
      return 200
    } catch (error) {
      console.log(error)
      return 500
    }
  }

  async getData (key) {
    try {
      const data = await AsyncStorage.getItem(key)
      if (data !== null) {
        return data
      } else {
        return null
      }
    } catch (error) {
      console.log(error)
      return 500
    }
  }
}

export default new Store()