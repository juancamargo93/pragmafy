/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Provider, connect } from 'react-redux'
import { Provider as PaperProvider } from 'react-native-paper'
import AppReducer from './src/reducers/AppReducer'
import { AppNavigator, middleware } from './src/components/AppNavigator'
import { createStore, applyMiddleware } from 'redux'
import {
  reduxifyNavigator
} from 'react-navigation-redux-helpers'


const Root = reduxifyNavigator(AppNavigator, 'root')
const mapStateToProps = (state) => ({
  state: state.nav
})

const AppWithNavigationState = connect(mapStateToProps)(Root)

const store = createStore(
  AppReducer,
  applyMiddleware(middleware)
)

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <PaperProvider>
          <AppWithNavigationState />
        </PaperProvider>
      </Provider>
    )
  }
}

export default App
